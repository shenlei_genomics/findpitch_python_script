# -*- coding: utf-8 -*-
"""
Created on Fri Jul 27 09:42:44 2018

@author: shenlei
"""
from __future__ import print_function
import os.path


###### Version and Date
prog_version = '0.1.1'
prog_date = '2018-07-27'
##########################

class PitchRunner(object):
    """
    This class is designed to run different pitch by :
        1. select the A channel of FOV and detect its pitch
        2. call basecall by the pitch's setting accordingly.
    """
    def __init__(self,datapath, cycle = 100 ,  pecycle = None):
        self.findpitchexe=r'D:\SL\highDensity\FindPitchByFFT.exe'
        self.basecalldir = r'D:\LM\V2.4_4'
        #800nm, 720nm, 600nm, 576nm, 480nm, 450nm
        self.pitchdict = {0:{'settingfn':'settings800.config'},
        1:{'settingfn':'settings720.config'},
        2:{'settingfn':'settings600.config'},
        3:{'settingfn':'settings576.config'},
        4:{'settingfn':'settings480.config'},
        5:{'settingfn':'settings450.config'}}
        self.datapath = datapath
        self.cycle = cycle
        self.pecycle = pecycle
        self.FOVdict = {}
        self.getFOVPitch()
    
    def getFOVPitch(self):
        S001_dir = os.path.join(self.datapath, 'S001')
        if not os.path.exists(S001_dir):
            raise OSError
        import glob
        filelist = glob.glob(os.path.join(S001_dir, '*.A.*.tif'))
        import re
        pattern = '(C(\d+)R(\d+))'
        for file in filelist:
            ret = os.system('{0} {1}'.format(self.findpitchexe,file))
            if ret > -1:
                match = re.findall(pattern,file)
                if match:
                    assert(len(match[0]) == 3)
                    self.FOVdict[match[0][0]] = {'index':ret, 'col':int(match[0][1]), 'row':int(match[0][2])} 
    
    def runBasecall(self):
        os.chdir(self.basecalldir)
        for key in self.pitchdict.keys():
            os.system('start processor.exe {0}'.format(self.pitchdict[key]['settingfn']))
        for FOV in self.FOVdict.keys():
            index = self.FOVdict[FOV]['index']
            if self.pecycle is not None:
                print('client.exe {0} {1} {2} {3} -I {4} -P {5} -S -N C{2}R{3}'.format(self.datapath, self.cycle, self.FOVdict[FOV]['col'],self.FOVdict[FOV]['row'], self.pitchdict[index]['settingfn'], self.pecycle))
                os.system('client.exe {0} {1} {2} {3} -I {4} -P {5} -S -N C{2}R{3}'.format(self.datapath, self.cycle, self.FOVdict[FOV]['col'],self.FOVdict[FOV]['row'], self.pitchdict[index]['settingfn'], self.pecycle))
            else:
                print('client.exe {0} {1} {2} {3} -I {4} -S -N C{2}R{3}'.format(self.datapath, self.cycle, self.FOVdict[FOV]['col'],self.FOVdict[FOV]['row'], self.pitchdict[index]['settingfn'], self.pecycle))
                os.system('client.exe {0} {1} {2} {3} -I {4} -S -N C{2}R{3}'.format(self.datapath, self.cycle, self.FOVdict[FOV]['col'],self.FOVdict[FOV]['row'], self.pitchdict[index]['settingfn']))

#################################
##
##   Main function of program.
##
#################################
def main():
    import sys

    ###### Usage
    usage = '''

    Version %s  by Lei Shen  %s

    Usage: %s datapath [-c cycle] [-p pecycle] >STDOUT
    ''' % (prog_version, prog_date, os.path.basename(sys.argv[0]))
    
    ######################### Phrase parameters #########################
    import argparse
    ArgParser = argparse.ArgumentParser(usage=usage)
    ArgParser.add_argument("datapath", action='store', type=str, default = '',help="image data path. [%(default)s]")
    ArgParser.add_argument("--version", action="version", version=prog_version)
    ArgParser.add_argument("-c", "--cycle", action="store", dest='cycle', type=int, default = 100, metavar="INT", help="image cycle count. [%(default)s]")
    ArgParser.add_argument("-p","--pecycle", dest="pecycle", action='store', type=int, default = None,help="PE cycle. [%(default)s]")
    

    (para, args) = ArgParser.parse_known_args()

    if len(args) >0:
        ArgParser.print_help()
        print("\nERROR: The parameters number is not correct!", file=sys.stderr)
        sys.exit(1)
    else:
        pr = PitchRunner(para.datapath,cycle=para.cycle, pecycle=para.pecycle)
        pr.runBasecall()
#################################
##
##   Start the main program.
##
#################################
if __name__ == '__main__':
    main()

################## God's in his heaven, All's right with the world. ##################


            
    
    
    
        